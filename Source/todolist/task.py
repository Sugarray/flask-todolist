from flask import json

__author__ = 'Vladimir Volodavets'


class Task:
    """
    Class that represents Task in ToDo List

        :param task_id: key for redis storage

        :param description: description of Task

        :param deadline: time in HH-MM format

        :param importance: value of task importance

    """

    def __init__(self, task_id, description, deadline, importance):
        self.task_id = task_id
        self.description = description
        self.deadline = deadline
        self.importance = importance

    def __str__(self):
        """Returns information about task"""
        return "Task id: " + str(
            self.task_id) + "\n" + "Task description: " + self.description + \
                            "\n" + "Task deadline: " + self.deadline + "\n" + \
                            "Task importance: " + str(self.importance)

    def to_json(self):
        data = [{"task_id": self.task_id, "description": self.description,
                 "deadline": self.deadline,
                 "importance": self.importance}]

        js = json.dumps(data)
        return js
