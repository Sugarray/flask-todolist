import re
from flask import json

from Source.todolist.day import Day


__author__ = 'Vladimir Volodavets'

DATE_PATTERN = r"^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$"
DEADLINE_PATTERN = r"^(([01]\d|2[0-3]):([0-5]\d)|24:00)$"


class ToDoList:
    """ToDo List class"""

    def __init__(self):
        self.days = []

    def add_day(self, date):
        """
        Adds day in ToDo list

        :param date: date of day

        """

        self.validate_day(date)
        self.days.append(Day(date))

    def remove_day(self, date):
        """
        Removes day from ToDo list

        :param date: date of day

        """
        for i in self.days:
            if i.date == date:
                self.days.remove(i)

    def validate_day(self, date):
        """
        Validates day that will be added in ToDo List

        :param date: date of day

        """

        date_regex = re.compile(DATE_PATTERN)

        if not date:  # check if string is empty
            raise ValueError("Date field must be non empty")

        if not date_regex.match(date):  # match pattern
            raise ValueError("Day doesn't match pattern YYYY-MM-DD")

        for i in self.days:  # check if day exists
            if i.date == date:
                raise ValueError("Day with this date is already exist")

        return True

    def get_day_by_date(self, date):
        """Returns date by date

           :param date: date of day

           :returns: Day object
        """

        for i in self.days:
            if i.date == date:
                return i

    def to_json(self):

        array = []

        for i in self.days:
            array.append(i.to_json())

        js = json.dumps(array)
        return js