import re
import json

from Source.todolist.task import Task


__author__ = 'Vladimir Volodavets'


DEADLINE_PATTERN = r"^(([01]\d|2[0-3]):([0-5]\d)|24:00)$"


class Day:
    """
    Class that represents Day in ToDo List

        :param date: date in YYYY-MM-DD format

    """

    def __init__(self, date):
        self.id = 1
        self.date = date
        self.tasks = []

    def add_task(self, args):
        """
        Adds task for day

        :param args: Arguments for Task class

        """

        self.validate_task(args[0], args[1])  # description deadline
        self.tasks.append(Task(self.id, *args))
        self.id += 1

    def remove_task(self, task_id):
        """
        Removes task from day

        :param task_id: id of task which will be removed

        """

        for i in self.tasks:
            if i.task_id == task_id:
                self.tasks.remove(i)

    def validate_task(self, description, deadline):
        """
        Validates day that will be added in ToDo List

        :param description: description of task

        :param deadline: value of deadline

        """

        deadline_regex = re.compile(DEADLINE_PATTERN)

        if not description:
            raise ValueError("Description field must be non empty")

        if not deadline:
            raise ValueError("Deadline field must be non empty")

        if not deadline_regex.match(deadline):  # match pattern
            raise ValueError("Day doesn't match pattern HH:MM")

    def get_task_by_id(self, task_id):
        for i in self.tasks:
            if i.task_id == task_id:
                return i

    def to_json(self):

            tasks = {}
            j = 1
            for i in self.tasks:
                tasks[j] = i.to_json()
                j += 1

            data = [{"date": self.date, "tasks": tasks}]
            js = json.dumps(data)
            return js
