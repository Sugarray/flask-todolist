import json
from flask import Flask, request
from Source.todolist.manager import Manager


__author__ = 'Vladimir Volodavets'

if __name__ == '__main__':
    app = Flask(__name__)
    manager = Manager()

    @app.errorhandler(500)
    def page_not_found():
        return "Internal Server Error", 500

    @app.route('/todo/day/add_date', methods=['POST'])
    def add_day():
        post_data = json.loads(request.data)
        manager.add_day(post_data['date'])
        
        return "[1]"

    @app.route('/todo/day/<date>', methods=['GET', 'DELETE'])
    def get_day(date):
        if request.method == 'GET':
            return manager.get_single_day(date)
        else:
            manager.todo_list.remove_day(date)
            return "[1]"

    @app.route('/todo/day/<date>/add_task', methods=['POST'])
    def add_task(date):
        post_data = json.loads(request.data)
        day = manager.todo_list.get_day_by_date(date)
        args = (post_data['description'], post_data['deadline'],
                post_data['importance'])
        day.add_task(args)
        return "[1]"

    @app.route('/todo/day/<date>/task/<id>', methods=['GET', 'DELETE'])
    def get_task(date, id):
        day = manager.todo_list.get_day_by_date(date)
        if request.method == 'GET':
            task = day.get_task_by_id(int(id))
            return task.to_json()
        else:
            day.remove_task(int(id))
            return "[1]"

    @app.route('/todo')
    def get_todo():
        return manager.todo_list.to_json()

    @app.route('/save')
    def save():
        return manager.redis_export()

    manager.connect()
    manager.redis_import()

    app.run()
